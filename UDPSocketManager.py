import socket
from time import sleep


class UDPNetObj:

    def __init__(self, host, port):
        self.host = host

        # Port MUST be an int
        if type(port) == type("str"):
            self.port = int(port)

        # since is always UDP transport
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    def close(self):
        self.sock.close()


class Client(UDPNetObj):

    def send(self, data):

        # MUST send byte-type
        if type(data) != type(b""):
            # Adding \n in the end
            if not data.endswith("\n"): data += "\n"
            self.sock.sendto(data.encode('utf-8'), (self.host, self.port))
        else:
            # Adding \n in the end
            if not data.endswith(b"\n"): data += b"\n"
            self.sock.sendto(data, (self.host, self.port))


class Server(UDPNetObj):

    def __init__(self, listen_port):
        self.host = "localhost"

        # Port MUST be an int
        if type(listen_port) == type("str"):
            self.listen_port = int(listen_port)

        # Auto bind socket
        # self.sock.bind((self._host, self._port))

    def listen(self, timeout=30):
        # def timeout 30 secs
        while timeout > 0:
            msg, addr = self.recvfrom(1024)
            print(msg, addr)
            sleep(1)
            timeout -= 1